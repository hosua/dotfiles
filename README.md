## Disclaimer 

These configs arent really intended for anyone else to use but by all means, go ahead if you feel like it.


## Initial setup
```
Put scripts repository in ~/Scripts/
Put dwmblocks-dmenu repository in ~/Sources/
Move all files in dotfiles repository to ~/
Add "~/Scripts/Path Scripts" to path
```

## Vim stuff

When running vim for the first time, get vundle from the AUR first (or from wherever depending on your distro, 
then run ``:PluginInstall`` to install plugins in the vimrc

For Archlinux, you can get vundle and syntastic here. For other distrobutions, please refer to the Github links.

vim plugin manager (all plugins are already added in vimrc, just download this first)

[vundle-git](https://aur.archlinux.org/packages/vundle-git) [GitHub](https://github.com/VundleVim/Vundle.vim)

Syntax highlighting (no longer using youcompleteme)

[syntastic](https://aur.archlinux.org/packages/neovim-syntastic) [GitHub](https://github.com/vim-syntastic/syntastic)

Run ``python3 -m pip install --user --upgrade pynvim`` for python provider

You will also need the ``flake8`` and/or ``pylint``packages installed in order for syntastic's syntax highlighting to work. 

They may be named something else for other package managers.


## A bunch of other stuff
This is mainly here so that I can easily download it all again if I need to
```
xorg base-devel xorg-server git wget alsa-utils mesa-utils \
sxiv feh fzf htop bashtop dunst conky neofetch spectacle \
firefox discord thunar ranger python-pygments w3m zip \
neovim tmux vim code \
xorg-xbacklight xbindkeys xcompmgr xinput ttf-joypixels \
nm-applet cbatticon volumeicon network-manager-applet \
ark qbittorrent jdownloader2 lxappearance \ 
libreoffice virtualbox \
```

## AUR
Better screenshotter for xorg (I just use spectacle now, but this is a nice lightweight tool that works well with xorg)

[escrotum-git](https://aur.archlinux.org/packages/escrotum-git) [GitHub](https://github.com/Roger/escrotum)

Needed for fonts to display properly in dwm build

[nerd-fonts-complete](https://aur.archlinux.org/packages/nerd-fonts-complete) [GitHub](https://github.com/ryanoasis/nerd-fonts)

Also needed for font support 

[libfxt-bgra-git](https://aur.archlinux.org/packages/libxft-bgra-git) [GitHub](https://gitlab.freedesktop.org/xorg/lib/libxft.git)

surfshark VPN CLI

[surfshark](https://aur.archlinux.org/packages/surfshark-vpn) 

I no longer use tmux but I still occaisionally do when stuck in TTY.

[tmux-plugin-manager](https://aur.archlinux.org/packages/tmux-plugin-manager) [GitHub](https://github.com/tmux-plugins/tpm)

qt5 Power manager (I guess I needed this for my laptop)

[powerkit](https://aur.archlinux.org/packages/vundle-git) [GitHub](https://github.com/rodlie/powerkit)

I quit rs lol but just incase I come back

[runelite](https://aur.archlinux.org/packages/runelite) [GitHub](https://github.com/runelite)



## Other GitHub Links

[tmux-yank](https://github.com/tmux-plugins/tmux-yank)

[qbittorrent-themes](https://github.com/jagannatharjun/qbt-theme)
